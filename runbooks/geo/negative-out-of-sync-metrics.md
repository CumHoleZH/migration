# Negative out of sync metrics

## First and foremost

*Don't Panic*

## Symptoms

* The Geo status dashboard in Grafana shows negative numbers for the
Repositories or Wikis Out of Sync:

    ![Repos and wikis Out of Sync negative](../img/geo-negative-out-of-sync.png)

## Diagnose projects

* Get list of project IDs that should not exist in the tracking database:

    ```ruby
    project_ids = Project.pluck(:id) ; project_ids.count
    registry_projects = Geo::ProjectRegistry.pluck(:project_id) ; registry_projects.count
    should_not_be_present = (registry_projects - project_ids) ; should_not_be_present.count
    ```

The results are **project IDs** for `project_registry` rows that exist, but
should not (due to the project ID no longer existing in the database, which
signifies that the project has been removed).

Lag between database replication and Geo event log processing can account for
some of these outliers, but project IDs that persistently show in the list are
probably caused by the Geo secondary not receiving, losing, or failing to
process, a `Geo::RepositoryDeletedEvent` when a project is removed from the
primary.

Some snippets for root cause analysis:

* Find associated audit events:

    ```ruby
    AuditEvent.where(entity_type: 'Project', entity_id: should_not_be_present).where("details LIKE '%remove%project%'").count
    ```

    If no audit event exists, this suggests that a bug or other failure
    prevented the event log entry from being created.

* Find associated Geo Deleted Events:

    ```ruby
    Geo::RepositoryDeletedEvent.where(project_id: should_not_be_present).count
    ```

    Keep in mind, the Geo log events might be pruned.

## Run reconciliation

* Get the list of orphaned projects

    ```ruby
    project_ids = Project.pluck(:id) ; project_ids.count
    registry_projects = Geo::ProjectRegistry.pluck(:project_id) ; registry_projects.count
    should_not_be_present = (registry_projects - project_ids) ; should_not_be_present.count
    should_not_be_present
    ```

* Remove project_registry rows that should not be present

    ```ruby
    Geo::ProjectRegistry.where(project_id: should_not_be_present).count
    Geo::ProjectRegistry.where(project_id: should_not_be_present).delete_all
    ```

Note that data relating to the projects on the *filesystem* will not be removed.
This includes the project and wiki repository, and any *locally synchronized*
uploads. We have a [post-migration task](https://gitlab.com/gitlab-com/migration/issues/467)
to perform this cleanup, so it is not a large problem.

Uploads in object storage will have been removed by the primary, so will not be
orphaned by this process.

## Post diagnose

* Check the status again:

    ```ruby
    pp GeoNodeStatus.new(geo_node: Gitlab::Geo.current_node).load_data_from_current_node
    ```

## Related issues

gitlab-com/migration#295
